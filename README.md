![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

---

PlanetaLibre nace con la intención de seguir el gran proyecto de PlanetaLibre.es
creado y desarrollado por Jesús Camacho aka. Zagur.

Es un agregador de blogs y webs con temáticas sobre GNU/Linux y el software
libre.

---

El código está escrito por _Selairi_ en Python y disponible en
[GitHub](https://github.com/selairi/planetlibre) bajo licencia BSD.
De ese proyecto inicial he tomado la idea de utilizar el servicio GitLab Pages
de GitLab para hospedar esos _feeds_ a los blogs en un sencilla página html a la
que puedes acceder en este enlace:

* https://victorhck.gitlab.io/planetalibre/

Los _feeds_ se actualizan un par de veces al día mediante una tarea _cron_, en mi servidor personal formado por una Raspberry Pi.
También puedes recibir los RSS de PlanetaLibre en tu lector RSS favorito.

